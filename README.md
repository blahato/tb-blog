#TB BLOG

##Run project
To locally run project:

* BUILD: build the application using `composer dev`
* MYSQL: use local MySQL 5.7 or use `docker-compose up` 
to use the preconfigured docker-compose file
* DATABASE: create an empty database
* SETUP: edit .env file to match database credentials 
(for dockerized MySQL use root:root)
* DB SCHEMA: create an empty database schema using `bin/console do:sch:up --force`
* USER: create DEMO user using custom command `bin/console tbblog:demo:create-user EMAIL PASSOWRD`
* SERVER: run local server using `bin/console server:start`


Administration can be access via `(local)host(:8000)/admin`

API can be access via `(local)host(:8000)/api`


##Run tests
To run application CI check routine, use composer command, 
which consists of assets analysis, code style analysis, unit tests and integration tests.

`composer ci`
