<?php

declare(strict_types = 1);

namespace TbBlog;

abstract class IntegrationDatabaseSerialTestCase extends IntegrationDatabaseTestCase
{

    protected function setUp(): void
    {
        parent::setUp();
        self::rebuildDatabase(self::$kernel);
    }

}
