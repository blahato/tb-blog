<?php

declare(strict_types = 1);

namespace TbBlog;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase as SymfonyKernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class KernelTestCase extends SymfonyKernelTestCase
{

    protected static function getKernelClass(): string
    {
        return Kernel::class;
    }

    public static function getContainer(): ContainerInterface
    {
        if (self::$container === null) {
            self::bootKernel();
        }

        return self::$container;
    }

}
