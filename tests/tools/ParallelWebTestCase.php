<?php

declare(strict_types = 1);

namespace TbBlog;

use TbBlog\Fixtures\User\UserFixture;
use TbBlog\User\User;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

abstract class ParallelWebTestCase extends KernelTestCase
{

    /** @var bool */
    public static $hasDatabaseBeenBuilt = false;

    /**
     * @param string[] $options An array of options to pass to the createKernel class
     * @param string[] $server An array of server parameters
     *
     * @return \Symfony\Bundle\FrameworkBundle\Client A Client instance
     */
    protected static function createClient(array $options = [], array $server = []): Client
    {
        self::bootKernel($options);

        /** @var \Symfony\Bundle\FrameworkBundle\Client $client */
        $client = self::getContainer()->get('test.client');

        $client->setServerParameters($server);

        $client->disableReboot();

        return $client;
    }

    /**
     * @see https://symfony.com/doc/3.3/testing/http_authentication.html#creating-the-authentication-token
     * @param string[] $options An array of options to pass to the createKernel class
     * @param string[] $server An array of server parameters
     *
     * @return \Symfony\Bundle\FrameworkBundle\Client A Client instance
     */
    protected static function createAuthenticatedAdmin(array $options = [], array $server = []): Client
    {
        return self::createAuthenticatedClient(
            UserFixture::$admin,
            '_security_main',
            $options,
            $server
        );
    }

    protected static function getKernelClass(): string
    {
        return Kernel::class;
    }

    public static function setUpBeforeClass(): void
    {
        /** @var \Symfony\Bundle\FrameworkBundle\Client $client */
        $client = self::bootKernel()->getContainer()->get(Client::class);

        if (self::$hasDatabaseBeenBuilt === false) {
            IntegrationDatabaseParallelTestCase::rebuildDatabase($client->getKernel());
            self::$hasDatabaseBeenBuilt = true;
        }

        parent::setUpBeforeClass();
    }

    protected function setUp(): void
    {
        parent::setUp();
    }

    protected function reduceWhitespace(string $text): string
    {
        return trim(preg_replace('/\s+/', ' ', $text));
    }

    protected function getSecurityToken(Client $client): TokenInterface
    {
        /** @var \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage $tokenStorage */
        $tokenStorage = self::$container->get('security.token_storage');

        $securityToken = $tokenStorage->getToken();
        if ($securityToken === null) {
            throw new \Exception('Token cannot be null');
        }

        return $securityToken;
    }

    protected static function getContainer(): ContainerInterface
    {
        if (self::$container === null) {
            self::fail('Unable to retrieve container');
        }

        return self::$container;
    }

    /**
     * @see https://symfony.com/doc/3.3/testing/http_authentication.html#creating-the-authentication-token
     * @param \TbBlog\User\User $user
     * @param string $sessionName
     * @param string[] $options An array of options to pass to the createKernel class
     * @param string[] $server An array of server parameters
     *
     * @return \Symfony\Bundle\FrameworkBundle\Client A Client instance
     */
    private static function createAuthenticatedClient(User $user, string $sessionName, array $options = [], array $server = []): Client
    {
        $client = self::createClient($options, $server);

        /** @var \Symfony\Component\HttpFoundation\Session\Session $session */
        $session = self::$container->get('session');

        $token = new UsernamePasswordToken(
            $user,
            null,
            'main',
            $user->getRoles()
        );

        $session->set($sessionName, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $client->getCookieJar()->set($cookie);

        return $client;
    }

}
