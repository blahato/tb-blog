<?php

declare(strict_types = 1);

namespace TbBlog;

abstract class IntegrationTestCase extends KernelTestCase
{

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
    }

    protected function tearDown(): void
    {
        // prevent kernel shutdown
    }

    /**
     * @param string $name
     * @return mixed
     */
    protected function getService(string $name)
    {
        return self::getContainer()->get($name);
    }

    /**
     * @param string $type
     * @return mixed
     */
    protected function getServiceByType(string $type)
    {
        return $this->getService(self::convertClassNameToServiceName($type));
    }

    public static function convertClassNameToServiceName(string $className): string
    {
        $className = str_replace('\\', '.', $className);
        $className = preg_replace('/([a-z])([A-Z])/', '$1_$2', $className);
        $className = strtolower($className);

        return $className;
    }

}
