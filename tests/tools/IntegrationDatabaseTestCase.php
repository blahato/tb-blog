<?php

declare(strict_types = 1);

namespace TbBlog;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\StreamOutput;
use Symfony\Component\HttpKernel\KernelInterface;

class IntegrationDatabaseTestCase extends IntegrationTestCase
{

    /** @var bool */
    private static $dbInitDone = false;

    public static function rebuildDatabase(KernelInterface $kernel): void
    {
        self::initDatabase($kernel);

        self::runCommand(self::getApplication(self::bootKernel()), 'doctrine:fixtures:load', [
            '--no-interaction' => true,
        ]);
    }

    /**
     * @param \Symfony\Bundle\FrameworkBundle\Console\Application $application
     * @param string $command
     * @param mixed[] $arguments key value array of arguments
     * @return int
     */
    public static function runCommand(Application $application, string $command, array $arguments = []): int
    {
        $outputStream = fopen('php://temp', 'r+');
        self::assertNotFalse($outputStream);

        $arguments['--env'] = 'test';
        $arguments = array_merge(['command' => $command], $arguments);

        $return = $application->run(
            new ArrayInput($arguments),
            new StreamOutput($outputStream, StreamOutput::VERBOSITY_VERY_VERBOSE)
        );

        if ($return !== 0) {
            rewind($outputStream);
            $errors = stream_get_contents($outputStream);
            fclose($outputStream);
            if ($errors === false) {
                self::fail('Invalid output stream');
            }
            throw new \RuntimeException($errors);
        }
        fclose($outputStream);

        return $return;
    }

    public static function getApplication(KernelInterface $kernel): Application
    {
        $application = new Application($kernel);
        $application->setAutoExit(false);
        $application->setCatchExceptions(true);

        return $application;
    }

    private static function initDatabase(KernelInterface $kernel): void
    {
        if (self::$dbInitDone) {
            return;
        }

        self::runCommand(self::getApplication(self::bootKernel()), 'doctrine:database:create', [
            '--if-not-exists' => true,
            '--connection' => 'default',
        ]);

        self::runCommand(self::getApplication(self::bootKernel()), 'doctrine:schema:drop', [
            '--full-database' => true,
            '--force' => true,
            '--em' => 'default',
        ]);

        self::runCommand(self::getApplication(self::bootKernel()), 'doctrine:schema:create', [
            '--em' => 'default',
        ]);

        self::$dbInitDone = true;
    }

}
