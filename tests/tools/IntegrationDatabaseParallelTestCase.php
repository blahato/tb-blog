<?php

declare(strict_types = 1);

namespace TbBlog;

abstract class IntegrationDatabaseParallelTestCase extends IntegrationDatabaseTestCase
{

    public static function setUpBeforeClass(): void
    {
        self::bootKernel();
        /** @var \Symfony\Bundle\FrameworkBundle\Client $client */
        $client = self::getContainer()->get('test.client');
        self::rebuildDatabase($client->getKernel());
        parent::setUpBeforeClass();
    }

    protected function setUp(): void
    {
        // prevent kernel startup on each single test
    }

}
