<?php

declare(strict_types = 1);

namespace TbBlog\Fixtures\User;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;
use Tbblog\User\User;
use TbBlog\User\UserRole;

class UserFixture extends Fixture
{

    public static $admin;
    public const ADMIN_REFERENCE = 'admin-reference';

    public function load(ObjectManager $manager)
    {

        $admin = new User(
            Uuid::uuid4(),
            'tomas.blaha@outlook.com',
            '$2y$10$mnzjGs7oFc6Dqr0i0BO89euJQ.0MY0/5ryRJaDPCgBO9oFCGNiSzu', // Heslo123
            new \DateTimeImmutable('2019-01-01'),
            UserRole::get(UserRole::ADMIN)
        );

        $manager->persist($admin);

        $manager->flush();

        self::$admin = $admin;
        $this->addReference(self::ADMIN_REFERENCE, $admin);

    }

}
