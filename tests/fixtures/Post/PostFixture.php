<?php

declare(strict_types = 1);

namespace TbBlog\Fixtures\Post;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;
use TbBlog\Fixtures\Post\Tag\TagFixture;
use TbBlog\Post\Post;
use TbBlog\Post\Tag\TagList;
use Tbblog\User\User;
use TbBlog\User\UserRole;

class PostFixture extends Fixture
{

    /** @var \TbBlog\Post\Post */
    public static $publishedPost;
    public const PUBLISHED_POST_REFERENCE = 'published-post-reference';

    /** @var \TbBlog\Post\Post */
    public static $deletedPost;
    public const DELETED_POST_REFERENCE = 'deleted-post-reference';

    public function load(ObjectManager $manager)
    {
        $publishedPost = new Post(
            Uuid::uuid4(),
            'Test post',
            '<h1>Just a test post</h1>',
            new TagList([
                $this->getReference(TagFixture::IMPORTANT_TAG_REFERENCE),
            ]),
            'test-post',
            new \DateTimeImmutable('2019-01-01')
        );

        $manager->persist($publishedPost);

        $deletedPost = new Post(
            Uuid::uuid4(),
            'Test post',
            '<h1>Deleted post</h1>',
            new TagList([
                $this->getReference(TagFixture::IMPORTANT_TAG_REFERENCE),
            ]),
            'deleted-post',
            new \DateTimeImmutable('2019-01-01')
        );

        $deletedPost->setDeletedAt(
            new \DateTimeImmutable('2019-02-01')
        );

        $manager->persist($deletedPost);
        $manager->flush();

        self::$publishedPost = $publishedPost;
        $this->addReference(self::PUBLISHED_POST_REFERENCE, $publishedPost);

        self::$deletedPost = $deletedPost;
        $this->addReference(self::DELETED_POST_REFERENCE, $deletedPost);
    }

}
