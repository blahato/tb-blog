<?php

declare(strict_types = 1);

namespace TbBlog\Fixtures\Post\Tag;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;
use TbBlog\Post\Post;
use TbBlog\Post\Tag\Tag;
use TbBlog\Post\Tag\TagList;
use Tbblog\User\User;
use TbBlog\User\UserRole;

class TagFixture extends Fixture
{

    public static $importantTag;
    public const IMPORTANT_TAG_REFERENCE = 'important-tag-reference';

    public function load(ObjectManager $manager)
    {
        $importantTag = new Tag(
            Uuid::uuid4(),
            'Important',
            'important',
            new \DateTimeImmutable('2019-01-01')
        );

        $manager->persist($importantTag);
        $manager->flush();

        self::$importantTag = $importantTag;
        $this->addReference(self::IMPORTANT_TAG_REFERENCE, $importantTag);

    }

}
