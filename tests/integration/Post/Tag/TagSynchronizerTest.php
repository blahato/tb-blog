<?php

declare(strict_types = 1);

namespace TbBlog;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use TbBlog\Post\Tag\Tag;
use TbBlog\Post\Tag\TagFixture;
use TbBlog\Post\Tag\TagSynchonizer;

class TagSynchronizerTest extends IntegrationDatabaseSerialTestCase
{

    /** @var \TbBlog\Post\Tag\TagSynchonizer */
    private $tagSynchronizer;

    /** @var \Doctrine\ORM\EntityManagerInterface */
    private $entityManager;

    public function setUp(): void
    {
        parent::setUp();

        $this->tagSynchronizer = $this->getService(TagSynchonizer::class);
        $this->entityManager = $this->getService(EntityManagerInterface::class);
    }

    public function testSynchronizeTagWithDatabaseCreatesTag(): void
    {
        $newTag = $this->tagSynchronizer->synchronizeTagWithDatabase('New tag');
        self::assertTrue(
            $this->entityManager->contains($newTag)
        );
    }

    public function testSynchronizeTagWithDatabaseGetsTag(): void
    {
        $existingTag = $this->tagSynchronizer->synchronizeTagWithDatabase(
            TagFixture::createImportantTag()->getName()
        );
        self::assertTrue(
            $this->entityManager->contains($existingTag)
        );
    }

}
