<?php

declare(strict_types = 1);

namespace TbBlog\App\Post;

use TbBlog\ParallelWebTestCase;

class PostListControllerTest extends ParallelWebTestCase
{

    public function testPostListController(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        self::assertSame(200, $client->getResponse()->getStatusCode());
        self::assertSame(
            1,
            $crawler->filter('.card')->count()
        );
    }

}
