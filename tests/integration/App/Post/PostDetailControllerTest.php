<?php

declare(strict_types = 1);

namespace TbBlog\App\Post;

use TbBlog\Fixtures\Post\PostFixture;
use TbBlog\ParallelWebTestCase;

class PostDetailControllerTest extends ParallelWebTestCase
{

    public function testPostListController(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/post/' . PostFixture::$publishedPost->getSlug() . '/');

        self::assertSame(200, $client->getResponse()->getStatusCode());
        self::assertSame(
            PostFixture::$publishedPost->getTitle(),
            $crawler->filter('h1')->text()
        );
    }

    public function testPostListControllerDoesNotShowDeletedPost(): void
    {
        $client = static::createClient();
        $client->request('GET', '/post/' . PostFixture::$deletedPost->getSlug() . '/');

        self::assertSame(404, $client->getResponse()->getStatusCode());
    }

}
