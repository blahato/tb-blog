<?php

declare(strict_types = 1);

namespace TbBlog\Admin\Post;

use TbBlog\ParallelWebTestCase;

class PostListControllerTest extends ParallelWebTestCase
{

    public function testPostListController(): void
    {
        $client = static::createAuthenticatedAdmin();
        $crawler = $client->request('GET', '/admin/');

        self::assertSame(200, $client->getResponse()->getStatusCode());
        self::assertSame(
            2,
            $crawler->filter('table tbody tr')->count()
        );
    }

}
