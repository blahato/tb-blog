<?php

declare(strict_types = 1);

namespace TbBlog\Api\Post;

use TbBlog\Fixtures\Post\PostFixture;
use TbBlog\ParallelWebTestCase;

class PostDetailEndpointControllerTest extends ParallelWebTestCase
{

    public function testPostListController(): void
    {
        $client = static::createClient();
        $client->request('GET', '/api/post/' . PostFixture::$publishedPost->getSlug() . '/');

        self::assertSame(200, $client->getResponse()->getStatusCode());
        self::assertSame(
            $this->getExpectedJsonContent(),
            $client->getResponse()->getContent()
        );
    }

    public function testPostListControllerDoesNotShowDeletedPost(): void
    {
        $client = static::createClient();
        $client->request('GET', '/api/post/' . PostFixture::$deletedPost->getSlug() . '/');

        self::assertSame(404, $client->getResponse()->getStatusCode());
    }

    private function getExpectedJsonContent(): string
    {
        return '{"slug":"test-post","title":"Test post","text":"\u003Ch1\u003EJust a test post\u003C\/h1\u003E","createdTime":"2019-01-01 00:00","tags":[{"slug":"important","name":"Important"}]}';
    }

}
