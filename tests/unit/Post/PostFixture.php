<?php

declare(strict_types = 1);

namespace TbBlog\Post;

use Ramsey\Uuid\Uuid;
use TbBlog\Post\Tag\TagFixture;
use TbBlog\Post\Tag\TagList;

class PostFixture
{

    public static function createPost(): Post
    {
        return new Post(
            Uuid::fromString('aa153fcc-e506-4fe0-867a-28f248e74eba'),
            'Test post',
            '<p>Test post content</p>',
            new TagList([
                TagFixture::createAdTag(),
                TagFixture::createImportantTag(),
            ]),
            'test-post',
            new \DateTimeImmutable('2019-01-01')
        );
    }

    public static function createOlderPost(): Post
    {
        return new Post(
            Uuid::fromString('4bb1f561-46cb-4872-a2e5-feed0c8b7947'),
            'Old post',
            '<p>Old post content</p>',
            new TagList([
                TagFixture::createAdTag(),
            ]),
            'old-post',
            new \DateTimeImmutable('2018-01-01')
        );
    }

}
