<?php

declare(strict_types = 1);

namespace TbBlog\Post;

use PHPUnit\Framework\TestCase;
use TbBlog\Post\Tag\TagFixture;
use TbBlog\Post\Tag\TagList;

class PostFactoryTest extends TestCase
{

    /** @var \TbBlog\Post\PostFactory */
    private $postFactory;

    public function setUp(): void
    {
        parent::setUp();
        $this->postFactory = new PostFactory();
    }

    public function testCreatePost(): void
    {
        $post = $this->postFactory->createPost(
            'Test title',
            'Test text',
            new TagList([
                TagFixture::createAdTag(),
            ]),
            'test-slug',
            new \DateTimeImmutable('2019-01-01')
        );

        self::assertSame(
            'Test title',
            $post->getTitle()
        );

        self::assertSame(
            'Test text',
            $post->getText()
        );

        self::assertCount(
            1,
            $post->getTags()->getTags()
        );

        self::assertSame(
            '2019-01-01',
            $post->getCreatedTime()->format('Y-m-d')
        );

        self::assertFalse(
            $post->isDeleted()
        );
    }
}
