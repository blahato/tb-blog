<?php

declare(strict_types = 1);

namespace TbBlog\Post\Tag;


use PHPUnit\Framework\TestCase;

class TagListSerializerTest extends TestCase
{

    /** @var \TbBlog\Post\Tag\TagListSerializer */
    private $tagListSerializer;

    public function setUp(): void
    {
        parent::setUp();
        $this->tagListSerializer = new TagListSerializer(
            new TagSerializer()
        );
    }

    public function testSerializeTagList(): void
    {
        $expectedResult = [
            [
                'slug' => 'advertising',
                'name' => 'Advertising',
            ],
            [
                'slug' => 'important',
                'name' => 'Important',
            ],
        ];

        self::assertSame(
            $expectedResult,
            $this->tagListSerializer->serializeTagList(
                new TagList([
                    TagFixture::createAdTag(),
                    TagFixture::createImportantTag(),
                ])
            )
        );
    }
}
