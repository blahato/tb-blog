<?php

declare(strict_types = 1);

namespace TbBlog\Post\Tag;

use PHPUnit\Framework\TestCase;

class TagSerializerTest extends TestCase
{

    /** @var \TbBlog\Post\Tag\TagSerializer */
    private $tagSerializer;

    public function setUp(): void
    {
        parent::setUp();
        $this->tagSerializer = new TagSerializer();
    }

    public function testSerializeTag(): void
    {
        $expectedResult = [
            'slug' => 'advertising',
            'name' => 'Advertising',
        ];

        self::assertSame(
            $expectedResult,
            $this->tagSerializer->serializeTag(
                TagFixture::createAdTag()
            )
        );
    }
}
