<?php

declare(strict_types = 1);

namespace TbBlog\Post\Tag;

use Ramsey\Uuid\Uuid;

class TagFixture
{

    public static function createAdTag(): Tag
    {
        return new Tag(
            Uuid::fromString('a7316b2d-b90d-49a1-b7a9-91436f1a59bf'),
            'Advertising',
            'advertising',
            new \DateTimeImmutable('2019-01-01')
        );
    }

    public static function createImportantTag(): Tag
    {
        return new Tag(
            Uuid::fromString('c9131d5e-dbbb-4bac-8501-2d5dadf69493'),
            'Important',
            'important',
            new \DateTimeImmutable('2019-01-01')
        );
    }

}
