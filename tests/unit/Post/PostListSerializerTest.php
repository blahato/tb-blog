<?php

declare(strict_types = 1);

namespace TbBlog\Post;

use PHPUnit\Framework\TestCase;
use TbBlog\Post\Tag\TagListSerializer;
use TbBlog\Post\Tag\TagSerializer;

class PostListSerializerTest extends TestCase
{

    private $postListSerializer;

    public function setUp(): void
    {
        parent::setUp();
        $this->postListSerializer = new PostListSerializer(
            new PostSerializer(
                new TagListSerializer(
                    new TagSerializer()
                )
            )
        );
    }

    public function testSerializePostList(): void
    {
        $expectedResult = [
            [
                'slug' => 'test-post',
                'title' => 'Test post',
                'text' => '<p>Test post content</p>',
                'createdTime' => '2019-01-01 00:00',
                'tags' => [
                    [
                        'slug' => 'advertising',
                        'name' => 'Advertising',
                    ],
                    [
                        'slug' => 'important',
                        'name' => 'Important',
                    ],
                ],
            ],
            [
                'slug' => 'old-post',
                'title' => 'Old post',
                'text' => '<p>Old post content</p>',
                'createdTime' => '2018-01-01 00:00',
                'tags' => [
                    [
                        'slug' => 'advertising',
                        'name' => 'Advertising',
                    ],
                ],
            ],
        ];

        self::assertSame(
            $expectedResult,
            $this->postListSerializer->serializePostList(
                new PostList([
                    PostFixture::createPost(),
                    PostFixture::createOlderPost(),
                ])
            )
        );
    }

}
