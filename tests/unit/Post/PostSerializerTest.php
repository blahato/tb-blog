<?php

declare(strict_types = 1);

namespace TbBlog\Post;

use PHPUnit\Framework\TestCase;
use TbBlog\Post\Tag\TagListSerializer;
use TbBlog\Post\Tag\TagSerializer;

class PostSerializerTest extends TestCase
{

    /** @var \TbBlog\Post\PostSerializer */
    private $postSerializer;

    public function setUp(): void
    {
        parent::setUp();
        $this->postSerializer = new PostSerializer(
            new TagListSerializer(
                new TagSerializer()
            )
        );
    }

    public function testSerializePost(): void
    {
        $expectedResult = [
            'slug' => 'test-post',
            'title' => 'Test post',
            'text' => '<p>Test post content</p>',
            'createdTime' => '2019-01-01 00:00',
            'tags' => [
                [
                    'slug' => 'advertising',
                    'name' => 'Advertising',
                ],
                [
                    'slug' => 'important',
                    'name' => 'Important',
                ]
            ]
        ];

        self::assertSame(
            $expectedResult,
            $this->postSerializer->serializePost(
                PostFixture::createPost()
            )
        );
    }

}
