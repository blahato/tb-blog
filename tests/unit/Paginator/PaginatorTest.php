<?php

declare(strict_types = 1);

namespace TbBlog\Paginator;

use PHPUnit\Framework\TestCase;

class PaginatorTest extends TestCase
{

    /**
     * @dataProvider provideNumberOfPages
     * @param int $numberOfPages
     * @param \TbBlog\Paginator\Paginator $paginator
     */
    public function testGetNumberOfPages(
        int $numberOfPages,
        Paginator $paginator
    ): void
    {
        self::assertSame(
            $numberOfPages,
            $paginator->getNumberOfPages()
        );
    }

    /**
     * @dataProvider provideCurrentPageOffset
     * @param int $currentPageOffset
     * @param \TbBlog\Paginator\Paginator $paginator
     */
    public function testGetCurrentPageOffset(
        int $currentPageOffset,
        Paginator $paginator
    ): void
    {
        self::assertSame(
            $currentPageOffset,
            $paginator->getCurrentPageOffset()
        );
    }

    public function provideNumberOfPages(): array
    {
        return [
            [
                1,
                new Paginator(
                    10,
                    10,
                    1
                ),
            ],
            [
                1,
                new Paginator(
                    1,
                    1,
                    1
                ),
            ],
            [
                1,
                new Paginator(
                    2,
                    10,
                    1
                ),
            ],
            [
                2,
                new Paginator(
                    11,
                    10,
                    1
                ),
            ],
            [
                1,
                new Paginator(
                    0,
                    10,
                    1
                ),
            ],
        ];
    }

    public function provideCurrentPageOffset(): array
    {
        return [
            [
                0,
                new Paginator(
                    10,
                    10,
                    1
                ),
            ],
            [
                10,
                new Paginator(
                    11,
                    10,
                    2
                ),
            ],
        ];
    }

}
