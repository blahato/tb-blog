<?php

declare(strict_types = 1);

namespace TbBlog\Admin\User\Login;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LoginCheckController extends AbstractController
{

    /**
     * @Route("/login-check/", name="login_check")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(): Response
    {
        return $this->redirectToRoute('post-list');
    }

}
