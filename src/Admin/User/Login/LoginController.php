<?php

declare(strict_types = 1);

namespace TbBlog\Admin\User\Login;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use TbBlog\User\AuthorizationHelper;

class LoginController extends AbstractController
{

    /** @var \Symfony\Component\Security\Csrf\CsrfTokenManagerInterface */
    private $csrfTokenManager;

    /** @var \TbBlog\User\AuthorizationHelper */
    private $authorizationHelper;

    public function __construct(
        CsrfTokenManagerInterface $csrfTokenManager,
        AuthorizationHelper $authorizationHelper
    )
    {
        $this->csrfTokenManager = $csrfTokenManager;
        $this->authorizationHelper = $authorizationHelper;
    }

    /**
     * @Route("/prihlaseni/", name="login")
     * @param \Symfony\Component\Security\Http\Authentication\AuthenticationUtils $authenticationUtils
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(
        AuthenticationUtils $authenticationUtils
    ): Response
    {
        if ($this->authorizationHelper->isLoggedInUser() === true) {
            $this->redirectToRoute('post-list');
        }

        $loginForm = $this->createForm(
            LoginFormType::class,
            [
                '_csrf_token' => $this->csrfTokenManager->getToken('authenticate')->getValue(),
                '_username' => $authenticationUtils->getLastUsername(),
            ]
        );

        return $this->render(
            '@Admin/user/login.html.twig',
            [
                'loginForm' => $loginForm->createView(),
                'loginError' => $authenticationUtils->getLastAuthenticationError(),
            ]
        );
    }

}
