<?php

declare(strict_types = 1);

namespace TbBlog\Admin\User\Login;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\RouterInterface;

class LoginFormType extends AbstractType
{

    private const NO_FORM_PREFIX = '';

    /** @var \Symfony\Component\Routing\RouterInterface $router */
    private $router;

    public function __construct(
        RouterInterface $router
    )
    {
        $this->router = $router;
    }

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param string[] $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->setAction($this->router->generate('login_check'))
            ->add('_csrf_token', HiddenType::class)
            ->add('_username', EmailType::class, ['label' => 'E-mail'])
            ->add('_password', PasswordType::class, ['label' => 'Heslo'])
            ->add('_remember_me', HiddenType::class, [
                'attr' => [
                    'value' => '1',
                ],
            ])
            ->add('send', SubmitType::class, [
                'label' => 'Login',
                'attr' => [
                    'class' => 'btn btn-primary',
                ],
            ]);
    }

    // @codingStandardsIgnoreStart no return typehints, must match the interface
    public function getBlockPrefix() // @codingStandardsIgnoreEnd
    {
        return self::NO_FORM_PREFIX;
    }

}
