<?php

declare(strict_types = 1);

namespace TbBlog\Admin\User\Login;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class LogoutController extends AbstractController
{

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction(): void
    {
    }

}
