<?php

declare(strict_types = 1);

namespace TbBlog\Admin\Post;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use TbBlog\Post\Post;

/**
 * @Route("/admin")
 */
class EditPostController extends AbstractController
{

    /** @var \TbBlog\Admin\Post\EditPostControllerFacade */
    private $editPostControllerFacade;

    public function __construct(
        EditPostControllerFacade $editPostControllerFacade
    )
    {
        $this->editPostControllerFacade = $editPostControllerFacade;
    }

    /**
     * @Route("/post/{id}/edit/", name="edit-post")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \TbBlog\Post\Post $post
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(
        Request $request,
        Post $post
    ): Response
    {
        $editPostRequest = EditPostRequest::createFromPost($post);

        $editPostForm = $this->createForm(
            PostFormType::class,
            $editPostRequest
        );
        $editPostForm->handleRequest($request);

        if ($editPostForm->isSubmitted() && $editPostForm->isValid()) {
            $post = $this->editPostControllerFacade->processPostRequest(
                $post,
                $editPostRequest
            );
            $this->addFlash(
                'success',
                sprintf(
                    'Post "%s" was successfully updated',
                    $post->getTitle()
                )
            );

            return $this->redirectToRoute('post-list');
        }

        return $this->render(
            '@Admin/post/edit.html.twig',
            [
                'editPostForm' => $editPostForm->createView(),
                'post' => $post,
            ]
        );
    }

}
