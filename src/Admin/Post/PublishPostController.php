<?php

declare(strict_types = 1);

namespace TbBlog\Admin\Post;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use TbBlog\Post\Post;
use TbBlog\Post\PostFacade;

/**
 * @Route("/admin")
 */
class PublishPostController extends AbstractController
{

    /** @var \TbBlog\Post\PostFacade */
    private $postFacade;

    public function __construct(
        PostFacade $postFacade
    )
    {
        $this->postFacade = $postFacade;
    }

    /**
     * @Route("/post/{id}/publish/", name="publish-post")
     * @param \TbBlog\Post\Post $post
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function publishAction(
        Post $post
    ): Response
    {
        $post = $this->postFacade->publishPost($post);

        $this->addFlash(
            'success',
            sprintf(
                'Post "%s" has been published',
                $post->getTitle()
            )
        );
        return $this->redirectToRoute('post-list');
    }

}
