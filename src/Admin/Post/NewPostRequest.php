<?php

declare(strict_types = 1);

namespace TbBlog\Admin\Post;

use DateTimeImmutable;
use Symfony\Component\Validator\Constraints as Assert;
use TbBlog\Post\Slug\Validation\UniqueSlug;
use TbBlog\String\StringList;

class NewPostRequest
{

    /**
     * @var string
     * @Assert\Length(
     *      max = 150,
     *      maxMessage = "Title cannot be longer than {{ limit }} characters"
     * )
     */
    public $title;

    /**
     * @var string
     * @UniqueSlug()
     */
    public $slug;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $text;

    /** @var \TbBlog\String\StringList */
    public $tags;

    /** @var \DateTimeImmutable */
    public $createdTime;

    public static function createWithPredefinedCreatedTime(
        DateTimeImmutable $createdTime
    ): self
    {
        $request = new self();

        $request->createdTime = $createdTime;

        return $request;
    }

    public function getDeserializedTags(): StringList
    {
        return $this->tags === null ? new StringList([]) : $this->tags;
    }

}
