<?php

declare(strict_types = 1);

namespace TbBlog\Admin\Post;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Tuscanicz\DateTimeBundle\DateTimeFactory;

/**
 * @Route("/admin")
 */
class NewPostController extends AbstractController
{

    /** @var \TbBlog\Admin\Post\NewPostControllerFacade */
    private $newPostControllerFacade;

    /** @var \Tuscanicz\DateTimeBundle\DateTimeFactory */
    private $dateTimeFactory;

    public function __construct(
        NewPostControllerFacade $newPostControllerFacade,
        DateTimeFactory $dateTimeFactory
    )
    {
        $this->newPostControllerFacade = $newPostControllerFacade;
        $this->dateTimeFactory = $dateTimeFactory;
    }

    /**
     * @Route("/new-post/", name="new-post")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request): Response
    {
        $newPostRequest = NewPostRequest::createWithPredefinedCreatedTime(
            $this->dateTimeFactory->now()->toDateTimeImmutable()
        );

        $newPostForm = $this->createForm(
            PostFormType::class,
            $newPostRequest
        );
        $newPostForm->handleRequest($request);

        if ($newPostForm->isSubmitted() && $newPostForm->isValid()) {
            $post = $this->newPostControllerFacade->processPostRequest($newPostRequest);
            $this->addFlash(
                'success',
                sprintf(
                    'Post "%s" was successfully created',
                    $post->getTitle()
                )
            );

            return $this->redirectToRoute('post-list');
        }

        return $this->render(
            '@Admin/post/new.html.twig',
            [
                'newPostForm' => $newPostForm->createView(),
            ]
        );
    }

}
