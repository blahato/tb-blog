<?php

declare(strict_types = 1);

namespace TbBlog\Admin\Post;

use Consistence\Type\ArrayType\ArrayType;
use Symfony\Component\Validator\Constraints as Assert;
use TbBlog\Post\Post;
use TbBlog\Post\Slug\Validation\UniqueSlug;
use TbBlog\Post\Tag\Tag;
use TbBlog\String\StringList;

class EditPostRequest
{

    /** @var \TbBlog\Post\Post */
    public $post;

    /**
     * @var string
     * @Assert\Length(
     *      max = 150,
     *      maxMessage = "Title cannot be longer than {{ limit }} characters"
     * )
     */
    public $title;

    /**
     * @var string
     * @UniqueSlug()
     */
    public $slug;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $text;

    /** @var \TbBlog\String\StringList */
    public $tags;

    /** @var \DateTimeImmutable */
    public $createdTime;

    public static function createFromPost(Post $post): self
    {
        $request = new self();

        $stringTags = ArrayType::mapValuesByCallback(
            $post->getTags()->getTags(),
            function (Tag $tag): string {
                return $tag->getName();
            }
        );

        $request->post = $post;
        $request->title = $post->getTitle();
        $request->slug = $post->getSlug();
        $request->text = $post->getText();
        $request->tags = new StringList($stringTags);
        $request->createdTime = $post->getCreatedTime();

        return $request;
    }

    public function getDeserializedTags(): StringList
    {
        return $this->tags === null ? new StringList([]) : $this->tags;
    }

}
