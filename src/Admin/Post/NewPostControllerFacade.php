<?php

declare(strict_types = 1);

namespace TbBlog\Admin\Post;

use Cocur\Slugify\Slugify;
use TbBlog\Post\Post;
use TbBlog\Post\PostFactory;
use TbBlog\Post\PostRepository;
use TbBlog\Post\Tag\TagSynchonizer;

class NewPostControllerFacade
{

    /** @var \TbBlog\Post\PostFactory */
    private $postFactory;

    /** @var \TbBlog\Post\PostRepository */
    private $postRepository;

    /** @var \Cocur\Slugify\Slugify */
    private $slugify;

    /** @var \TbBlog\Post\Tag\TagSynchonizer */
    private $tagSynchonizer;

    public function __construct(
        PostFactory $postFactory,
        PostRepository $postRepository,
        Slugify $slugify,
        TagSynchonizer $tagSynchonizer
    )
    {
        $this->postFactory = $postFactory;
        $this->postRepository = $postRepository;
        $this->slugify = $slugify;
        $this->tagSynchonizer = $tagSynchonizer;
    }

    public function processPostRequest(NewPostRequest $postRequest): Post
    {
        $tagList = $this->tagSynchonizer->synchronizeTagListWithDatabase($postRequest->getDeserializedTags());

        $post = $this->postFactory->createPost(
            $postRequest->title,
            $postRequest->text,
            $tagList,
            $this->slugify->slugify($postRequest->slug),
            $postRequest->createdTime
        );

        return $this->postRepository->savePost($post);
    }

}
