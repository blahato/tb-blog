<?php

declare(strict_types = 1);

namespace TbBlog\Admin\Post;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use TbBlog\Post\PostFacade;

/**
 * @Route("/admin")
 */
class PostListController extends AbstractController
{

    /** @var \TbBlog\Post\PostFacade */
    private $postFacade;

    public function __construct(
        PostFacade $postFacade
    )
    {
        $this->postFacade = $postFacade;
    }

    /**
     * @Route("/", name="post-list")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(): Response
    {
        return $this->render(
            '@Admin/post/list.html.twig',
            [
                'postList' => $this->postFacade->getAllPosts(),
            ]
        );
    }

}
