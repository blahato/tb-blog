<?php

declare(strict_types = 1);

namespace TbBlog\Admin\Post;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use TbBlog\Ui\Form\Type\TagsType;

class PostFormType extends AbstractType
{

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param string[] $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Title',
                'required' => true,
            ])
            ->add('slug', TextType::class, [
                'label' => 'URL slug',
                'required' => true,
            ])
            ->add('text', TextareaType::class, [
                'label' => 'Text',
                'required' => false,
                'attr' => [
                    'class' => 'wysiwyg',
                ],
            ])
            ->add('tags', TagsType::class, [
                'label' => 'Tags',
                'required' => false,
                'attr' => [
                    'class' => 'tagsinput',
                ],
            ])
            ->add('createdTime', DateType::class, [
                'label' => 'Created at',
                'required' => true,
                'input' => 'datetime_immutable',
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Save',
            ]);
    }

}
