<?php

declare(strict_types = 1);

namespace TbBlog\Admin\Post;

use TbBlog\Post\Post;
use TbBlog\Post\PostFacade;
use TbBlog\Post\Tag\TagSynchonizer;

class EditPostControllerFacade
{

    /** @var \TbBlog\Post\Tag\TagSynchonizer */
    private $tagSynchonizer;

    /** @var \TbBlog\Post\PostFacade */
    private $postFacade;

    public function __construct(
        TagSynchonizer $tagSynchonizer,
        PostFacade $postFacade
    )
    {
        $this->tagSynchonizer = $tagSynchonizer;
        $this->postFacade = $postFacade;
    }

    public function processPostRequest(
        Post $post,
        EditPostRequest $editPostRequest
    ): Post
    {
        $tagList = $this->tagSynchonizer->synchronizeTagListWithDatabase($editPostRequest->getDeserializedTags());

        $post->setTitle($editPostRequest->title);
        $post->setSlug($editPostRequest->slug);
        $post->setText($editPostRequest->text);
        $post->setTags($tagList);
        $post->setCreatedTime($editPostRequest->createdTime);

        return $this->postFacade->savePost($post);
    }

}
