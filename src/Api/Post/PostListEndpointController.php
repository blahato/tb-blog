<?php

declare(strict_types = 1);

namespace TbBlog\Api\Post;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use TbBlog\Paginator\PaginatorFactory;
use TbBlog\Post\PostFacade;
use TbBlog\Post\PostListSerializer;
use TbBlog\Response\JsonResponseFactory;

/**
 * @Route("/api")
 */
class PostListEndpointController extends AbstractController
{

    private const POSTS_PER_PAGE = 10;

    /** @var \TbBlog\Post\PostFacade */
    private $postFacade;

    /** @var \TbBlog\Paginator\PaginatorFactory */
    private $paginatorFactory;

    /** @var \TbBlog\Response\JsonResponseFactory */
    private $jsonResponseFactory;

    /** @var \TbBlog\Post\PostListSerializer */
    private $postListSerializer;

    public function __construct(
        PostFacade $postFacade,
        PaginatorFactory $paginatorFactory,
        JsonResponseFactory $jsonResponseFactory,
        PostListSerializer $postListSerializer
    )
    {
        $this->postFacade = $postFacade;
        $this->paginatorFactory = $paginatorFactory;
        $this->jsonResponseFactory = $jsonResponseFactory;
        $this->postListSerializer = $postListSerializer;
    }

    /**
     * @Route("/{page}", name="api.post-list", defaults={"page"=1})
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(
        int $page
    ): Response
    {
        try {
            $paginator = $this->paginatorFactory->createPaginator(
                $this->postFacade->getPublicPostsCount(),
                self::POSTS_PER_PAGE,
                $page
            );
        } catch (\TbBlog\Paginator\Exception\NonExistingPageException $e) {
            return $this->jsonResponseFactory->createNotFoundResponse(
                $e
            );
        }

        return $this->jsonResponseFactory->createJsonOkResponse(
            $this->postListSerializer->serializePostList(
                $this->postFacade->getPublicPostsPaginated(
                    $paginator
                )
            )
        );
    }

}
