<?php

declare(strict_types = 1);

namespace TbBlog\Api\Post;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use TbBlog\Post\PostFacade;
use TbBlog\Post\PostSerializer;
use TbBlog\Post\PostVisit\PostVisitLogger;
use TbBlog\Response\JsonResponseFactory;

/**
 * @Route("/api")
 */
class PostDetailEndpointController extends AbstractController
{

    /** @var \TbBlog\Post\PostFacade */
    private $postFacade;

    /** @var \TbBlog\Response\JsonResponseFactory */
    private $jsonResponseFactory;

    /** @var \TbBlog\Post\PostSerializer */
    private $postSerializer;

    /** @var \TbBlog\Post\PostVisit\PostVisitLogger */
    private $postVisitLogger;

    public function __construct(
        PostFacade $postFacade,
        JsonResponseFactory $jsonResponseFactory,
        PostSerializer $postSerializer,
        PostVisitLogger $postVisitLogger
    )
    {
        $this->postFacade = $postFacade;
        $this->jsonResponseFactory = $jsonResponseFactory;
        $this->postSerializer = $postSerializer;
        $this->postVisitLogger = $postVisitLogger;
    }

    /**
     * @Route("/post/{slug}/", name="api.post-detail")
     * @param string $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(
        string $slug
    ): Response
    {
        try {
            $post = $this->postFacade->getPublicPostBySlug($slug);
        } catch (\TbBlog\Post\Exception\PostNotFoundBySlugException $e) {
            return $this->jsonResponseFactory->createNotFoundResponse($e);
        }

        $this->postVisitLogger->logApiVisit($post);
        return $this->jsonResponseFactory->createJsonOkResponse(
            $this->postSerializer->serializePost($post)
        );
    }

}
