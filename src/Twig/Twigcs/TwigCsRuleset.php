<?php

declare(strict_types = 1);

namespace TbBlog\Twig\Twigcs;

use Allocine\Twigcs\Rule\ArraySeparatorSpacing;
use Allocine\Twigcs\Rule\DelimiterSpacing;
use Allocine\Twigcs\Rule\HashSeparatorSpacing;
use Allocine\Twigcs\Rule\OperatorSpacing;
use Allocine\Twigcs\Rule\ParenthesisSpacing;
use Allocine\Twigcs\Rule\PunctuationSpacing;
use Allocine\Twigcs\Rule\SliceShorthandSpacing;
use Allocine\Twigcs\Rule\TernarySpacing;
use Allocine\Twigcs\Rule\TrailingSpace;
use Allocine\Twigcs\Rule\UnusedMacro;
use Allocine\Twigcs\Ruleset\RulesetInterface;
use Allocine\Twigcs\Validator\Violation;
use Allocine\Twigcs\Whitelist\TokenWhitelist;
use Twig_Token;

/**
 * The official twigcs ruleset, based on http://twig.sensiolabs.org/doc/coding_standards.html
 */
class TwigCsRuleset implements RulesetInterface
{

    /**
     * @return \Allocine\Twigcs\Rule\RuleInterface[]
     */
    public function getRules(): array
    {
        return [
            new DelimiterSpacing(Violation::SEVERITY_ERROR, 1),
            new ParenthesisSpacing(Violation::SEVERITY_ERROR, 0, 1),
            new ArraySeparatorSpacing(Violation::SEVERITY_ERROR, 0, 1),
            new HashSeparatorSpacing(Violation::SEVERITY_ERROR, 0, 1),
            new OperatorSpacing(Violation::SEVERITY_ERROR, [
                '==',
                '!=',
                '<',
                '>',
                '>=',
                '<=',
                '+',
                '-',
                '/',
                '*',
                '%',
                '//',
                '**',
                'not',
                'and',
                'or',
                '~',
                'is',
                'in',
            ], 1),
            new PunctuationSpacing(
                Violation::SEVERITY_ERROR,
                ['.', '..', '[', ']'],
                0,
                new TokenWhitelist([
                    ')',
                    Twig_Token::NAME_TYPE,
                    Twig_Token::NUMBER_TYPE,
                    Twig_Token::STRING_TYPE,
                ], [2])
            ),
            new PunctuationSpacing(
                Violation::SEVERITY_ERROR,
                ['|'],
                1
            ),
            new TernarySpacing(Violation::SEVERITY_ERROR, 1),
            new UnusedMacro(Violation::SEVERITY_INFO),
            new SliceShorthandSpacing(Violation::SEVERITY_ERROR),
            new TrailingSpace(Violation::SEVERITY_ERROR),
        ];
    }

}
