<?php

declare(strict_types = 1);

namespace TbBlog\Ui\Form\Type\TagsType;

use Consistence\Type\Type;
use Symfony\Component\Form\DataTransformerInterface;
use TbBlog\String\StringList;

class TagsTransformer implements DataTransformerInterface
{

    /**
     * @param \TbBlog\String\StringList|null $stringList
     * @return string|null
     */
    // @codingStandardsIgnoreStart no typehints, must match the interface
    public function transform($stringList): ?string // @codingStandardsIgnoreEnd
    {
        Type::checkType($stringList, StringList::class . '|null');

        if ($stringList === null) {
            return null;
        }

        return implode(',', $stringList->getStrings());
    }

    /**
     * @param string|null $tags
     * @return \TbBlog\String\StringList|null
     */
    // @codingStandardsIgnoreStart no typehints, must match the interface
    public function reverseTransform($tags): ?StringList // @codingStandardsIgnoreEnd
    {
        Type::checkType($tags, 'string|null');

        if ($tags === null) {
            return null;
        }

        return new StringList(explode(',', $tags));
    }

}
