<?php

declare(strict_types = 1);

namespace TbBlog\Ui\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use TbBlog\Ui\Form\Type\TagsType\TagsTransformer;

class TagsType extends AbstractType
{

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param string[] $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addModelTransformer(new TagsTransformer());
    }

    public function getParent(): string
    {
        return TextType::class;
    }

}
