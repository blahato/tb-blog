<?php

declare(strict_types = 1);

namespace TbBlog\App\Post;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use TbBlog\Post\PostFacade;
use TbBlog\Post\PostVisit\PostVisitLogger;

class PostDetailController extends AbstractController
{

    /** @var \TbBlog\Post\PostFacade */
    private $postFacade;

    /** @var \TbBlog\Post\PostVisit\PostVisitLogger */
    private $postVisitLogger;

    public function __construct(
        PostFacade $postFacade,
        PostVisitLogger $postVisitLogger
    )
    {
        $this->postFacade = $postFacade;
        $this->postVisitLogger = $postVisitLogger;
    }

    /**
     * @Route("/post/{slug}/", name="public.post-detail")
     * @param string $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(string $slug): Response
    {
        try {
            $post = $this->postFacade->getPublicPostBySlug($slug);
        } catch (\TbBlog\Post\Exception\PostNotFoundBySlugException $e) {
            throw $this->createNotFoundException();
        }

        $this->postVisitLogger->logWebVisit($post);

        return $this->render(
            '@App/post/detail.html.twig',
            [
                'post' => $post,
            ]
        );
    }

}
