<?php

declare(strict_types = 1);

namespace TbBlog\App\Post;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use TbBlog\Paginator\PaginatorFactory;
use TbBlog\Post\PostFacade;

class PostListController extends AbstractController
{

    private const POSTS_PER_PAGE = 2;

    /** @var \TbBlog\Post\PostFacade */
    private $postFacade;

    /** @var \TbBlog\Paginator\PaginatorFactory */
    private $paginatorFactory;

    public function __construct(
        PostFacade $postFacade,
        PaginatorFactory $paginatorFactory
    )
    {
        $this->postFacade = $postFacade;
        $this->paginatorFactory = $paginatorFactory;
    }

    /**
     * @Route("/{page}", name="public.post-list", defaults={"page"=1})
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(
        int $page
    ): Response
    {
        try {
            $paginator = $this->paginatorFactory->createPaginator(
                $this->postFacade->getPublicPostsCount(),
                self::POSTS_PER_PAGE,
                $page
            );
        } catch (\TbBlog\Paginator\Exception\NonExistingPageException $e) {
            throw $this->createNotFoundException();
        }

        return $this->render(
            '@App/post/list.html.twig',
            [
                'postList' => $this->postFacade->getPublicPostsPaginated(
                    $paginator
                ),
                'page' => $page,
                'numberOfPages' => $paginator->getNumberOfPages(),
            ]
        );
    }

}
