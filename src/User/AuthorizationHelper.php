<?php

declare(strict_types = 1);

namespace TbBlog\User;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class AuthorizationHelper
{

    /** @var \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface */
    private $tokenStorage;

    /** @var \TbBlog\User\UserFacade */
    private $userFacade;

    public function __construct(
        TokenStorageInterface $tokenStorage,
        UserFacade $userFacade
    )
    {
        $this->tokenStorage = $tokenStorage;
        $this->userFacade = $userFacade;
    }

    public function isLoggedInUser(): bool
    {
        try {
            $this->getCurrentUserFromTokenStorage();
        } catch (\TbBlog\User\Exception\UnauthorizedUserException $e) {
            return false;
        }

        return true;
    }

    public function getCurrentUser(): User
    {
        return $this->userFacade->getUserById(
            $this->getCurrentUserFromTokenStorage()->getId()
        );
    }

    private function getCurrentUserFromTokenStorage(): User
    {
        $token = $this->tokenStorage->getToken();
        if ($token === null) {
            throw new \TbBlog\User\Exception\UnauthorizedUserException(
                'Could not get token'
            );
        }
        $user = $token->getUser();
        if (is_string($user) === true) {
            throw new \TbBlog\User\Exception\UnauthorizedUserException(
                'Got anonymous user: ' . $user
            );
        }
        if ($user instanceof UserInterface === false) {
            throw new \TbBlog\User\Exception\UnauthorizedUserException(
                'Got unexpected object: ' . get_class($user)
            );
        }
        if ($user instanceof User === false) {
            throw new \TbBlog\User\Exception\UnauthorizedUserException(
                'Got unexpected user object: ' . get_class($user)
            );
        }

        return $user;
    }

}
