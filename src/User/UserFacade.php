<?php

declare(strict_types = 1);

namespace TbBlog\User;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;
use Tuscanicz\DateTimeBundle\DateTimeFactory;

class UserFacade
{

    /** @var \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface */
    private $tokenStorage;

    /** @var \Symfony\Component\EventDispatcher\EventDispatcherInterface */
    private $eventDispatcher;

    /** @var \Symfony\Component\HttpFoundation\RequestStack */
    private $requestStack;

    /** @var \TbBlog\User\UserRepository */
    private $userRepository;

    /** @var \Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder */
    private $passwordEncoder;

    /** @var \Tuscanicz\DateTimeBundle\DateTimeFactory */
    private $dateTimeFactory;

    public function __construct(
        TokenStorageInterface $tokenStorage,
        EventDispatcherInterface $eventDispatcher,
        RequestStack $requestStack,
        UserRepository $userRepository,
        BCryptPasswordEncoder $passwordEncoder,
        DateTimeFactory $dateTimeFactory
    )
    {
        $this->tokenStorage    = $tokenStorage;
        $this->eventDispatcher = $eventDispatcher;
        $this->requestStack    = $requestStack;
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->dateTimeFactory = $dateTimeFactory;
    }

    public function login(
        User $user
    ): void
    {
        $token = new UsernamePasswordToken(
            $user,
            $user->getPassword(),
            'main',
            $user->getRoles()
        );
        $this->tokenStorage->setToken($token);

        $request = $this->requestStack->getCurrentRequest();
        if ($request === null) {
            throw new \Exception('Invalid request');
        }
        $event = new InteractiveLoginEvent($request, $token);
        $this->eventDispatcher->dispatch(SecurityEvents::INTERACTIVE_LOGIN, $event);
    }

    public function createUser(
        string $email,
        string $password
    ): User
    {
        $salt = rand(0, 1000);
        $user = new User(
            Uuid::uuid4(),
            $email,
            $this->passwordEncoder->encodePassword($password, (string) $salt),
            $this->dateTimeFactory->now()->toDateTimeImmutable(),
            UserRole::get(UserRole::ADMIN)
        );

        $this->userRepository->saveUser($user);

        return $user;
    }

    public function getUserById(UuidInterface $id): User
    {
        return $this->userRepository->getUserById($id);
    }

    public function getUserByEmail(string $email): User
    {
        return $this->userRepository->getUserByEmail($email);
    }

    public function userExistsByEmail(string $email): bool
    {
        try {
            $this->getUserByEmail($email);
            return true;
        } catch (\Doctrine\ORM\NoResultException $e) {
            return false;
        }
    }

}
