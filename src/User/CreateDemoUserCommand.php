<?php

declare(strict_types = 1);

namespace TbBlog\User;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateDemoUserCommand extends Command
{

    private const ARGUMENT_EMAIL = 'email';
    private const ARGUMENT_PASSWORD = 'password';

    /** @var \TbBlog\User\UserRegistrationService */
    private $userRegistrationService;

    public function __construct(
        UserRegistrationService $userRegistrationService
    )
    {
        parent::__construct();
        $this->userRegistrationService = $userRegistrationService;
    }

    public function configure(): void
    {
        $this->setName('tbblog:demo:create-user');
        $this->setDescription('Creates user used for demonstration purposes');
        $this->addArgument(self::ARGUMENT_EMAIL, InputArgument::REQUIRED);
        $this->addArgument(self::ARGUMENT_PASSWORD, InputArgument::REQUIRED);
    }

    public function execute(InputInterface $input, OutputInterface $output): ?int
    {
        try {
            $user = $this->userRegistrationService->registerUser(
                $this->getTextArgument($input, self::ARGUMENT_EMAIL),
                $this->getTextArgument($input, self::ARGUMENT_PASSWORD)
            );

            $output->writeln(
                sprintf(
                    'User "%s" created',
                    $user->getEmail()
                )
            );

            return 0;
        } catch (\TbBlog\User\Exception\EmailRegisteredException $e) {
            $output->writeln(
                sprintf(
                    'E-mail "%s" is already used',
                    $this->getTextArgument($input, self::ARGUMENT_EMAIL),
                )
            );

            return 1;
        }
    }

    private function getTextArgument(
        InputInterface $input,
        string $argumentName
    ): string
    {
        $inputPart = $input->getArgument($argumentName);
        if (is_array($inputPart) || $inputPart === null) {
            throw new \InvalidArgumentException(
                sprintf(
                    'Arguments %s must be of type string',
                    $argumentName
                )
            );
        }

        return $inputPart;
    }

}
