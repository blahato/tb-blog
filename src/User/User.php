<?php

declare(strict_types = 1);

namespace TbBlog\User;

use Consistence\Doctrine\Enum\EnumAnnotation as Enum;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Serializable;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User implements UserInterface, Serializable, EquatableInterface
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     * @var \Ramsey\Uuid\UuidInterface
     */
    private $id;

    /**
     * @ORM\Column(type="string", options={"collation": "ascii_general_ci"}, unique=true)
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $password;

    /**
     * @Enum(class=UserRole::class)
     * @ORM\Column(type="string_enum")
     * @var \TbBlog\User\UserRole
     */
    private $userRole;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @var \DateTimeImmutable
     */
    private $createdTime;

    public function __construct(
        UuidInterface $uuid,
        string $email,
        string $passwordHash,
        DateTimeImmutable $createdTime,
        UserRole $userRole
    )
    {
        $this->id = $uuid;
        $this->email = $email;
        $this->password = $passwordHash;
        $this->createdTime = $createdTime;
        $this->userRole = $userRole;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */ // @codingStandardsIgnoreStart no typehints, must match the interface
    public function serialize(): string // @codingStandardsIgnoreEnd
    {
        return serialize([
            'id' => $this->id->toString(),
            'email' => $this->getUsername(),
            'password' => $this->getPassword(),
        ]);
    }

    /**
     * @param string $serialized
     * @return void
     */ // @codingStandardsIgnoreStart no typehints, must match the interface
    public function unserialize($serialized): void // @codingStandardsIgnoreEnd
    {
        $unserialized = unserialize($serialized, []);
        $this->id = Uuid::fromString($unserialized['id']);
        $this->email = $unserialized['email'];
        $this->password = $unserialized['password'];
    }

    /**
     * @return string[]
     */
    public function getRoles(): array
    {
        $role = $this->getUserRole();

        return [$role->getValue()];
    }

    public function getSalt(): ?string
    {
        return null;
    }

    public function getUsername(): string
    {
        return $this->getEmail();
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * Removes sensitive data from the user.
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials(): void
    {
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getUserRole(): UserRole
    {
        return $this->userRole;
    }

    public function getCreatedTime(): DateTimeImmutable
    {
        return $this->createdTime;
    }

    /**
     * The equality comparison should neither be done by referential equality
     * nor by comparing identities (i.e. getId() === getId()).
     *
     * However, you do not need to compare every attribute, but only those that
     * are relevant for assessing whether re-authentication is required.
     *
     * @param \Symfony\Component\Security\Core\User\UserInterface $user
     * @return bool
     */ // @codingStandardsIgnoreStart no typehints, must match the interface
    public function isEqualTo(UserInterface $user) // @codingStandardsIgnoreEnd
    {
        if ($this->getPassword() !== $user->getPassword()) {
            return false;
        }

        if ($this->getEmail() !== $user->getUsername()) {
            return false;
        }

        return true;
    }

}
