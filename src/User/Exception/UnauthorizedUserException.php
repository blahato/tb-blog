<?php

declare(strict_types = 1);

namespace TbBlog\User\Exception;

class UnauthorizedUserException extends \TbBlog\Exception\PhpException
{

    public function __construct(string $unauthorizedUserExceptionMessage, ?\Throwable $previous = null)
    {
        parent::__construct($unauthorizedUserExceptionMessage, $previous);
    }

}
