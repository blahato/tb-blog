<?php

declare(strict_types = 1);

namespace TbBlog\User;

class UserList
{

    /** @var \TbBlog\User\User[] */
    private $users;

    /**
     * @param \TbBlog\User\User[] $users
     */
    public function __construct(array $users)
    {
        $this->users = $users;
    }

    public function getUsersCount(): int
    {
        return count($this->users);
    }

    public function hasUsers(): bool
    {
        return $this->getUsersCount() > 0;
    }

    /**
     * @return \TbBlog\User\User[]
     */
    public function getUsers(): array
    {
        return $this->users;
    }

}
