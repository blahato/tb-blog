<?php

declare(strict_types = 1);

namespace TbBlog\User;

class UserRegistrationService
{

    /** @var \TbBlog\User\UserFacade */
    private $userFacade;

    public function __construct(
        UserFacade $userFacade
    )
    {
        $this->userFacade = $userFacade;
    }

    public function registerUser(
        string $email,
        string $password
    ): User
    {
        if ($this->userFacade->userExistsByEmail($email) === true) {
            throw new \TbBlog\User\Exception\EmailRegisteredException(
                sprintf(
                    'E-mail "%s" is already used',
                    $email
                )
            );
        }

        return $this->userFacade->createUser(
            $email,
            $password
        );
    }

}
