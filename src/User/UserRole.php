<?php

declare(strict_types = 1);

namespace TbBlog\User;

use Consistence\Enum\Enum;

class UserRole extends Enum
{

    public const ADMIN = 'ROLE_ADMIN';

}
