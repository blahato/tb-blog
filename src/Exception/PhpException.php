<?php

declare(strict_types = 1);

namespace TbBlog\Exception;

class PhpException extends \Exception
{

    public function __construct(string $message = '', ?\Throwable $previous = null)
    {
        parent::__construct($message, 0, $previous);
    }

}
