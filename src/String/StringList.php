<?php

declare(strict_types = 1);

namespace TbBlog\String;

class StringList
{

    /** @var string[] */
    private $strings;

    /**
     * @param string[] $strings
     */
    public function __construct(
        array $strings
    )
    {
        $this->strings = $strings;
    }

    public function getStringCount(): int
    {
        return count($this->strings);
    }

    public function hasString(): bool
    {
        return $this->getStringCount() > 0;
    }

    /**
     * @return string[]
     */
    public function getStrings(): array
    {
        return $this->strings;
    }

}
