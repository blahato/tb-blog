<?php

declare(strict_types = 1);

namespace TbBlog\Response;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class JsonResponseFactory
{

    /**
     * @param mixed[] $jsonData
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function createJsonOkResponse(
        array $jsonData
    ): JsonResponse
    {
        return new JsonResponse(
            $jsonData,
            Response::HTTP_OK
        );
    }

    public function createNotFoundResponse(
        \Throwable $e
    ): Response
    {
        return new JsonResponse(
            [
                'message' => $e->getMessage(),
            ],
            Response::HTTP_NOT_FOUND
        );
    }

}
