<?php

declare(strict_types = 1);

namespace TbBlog\Post;

use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use TbBlog\Post\Tag\TagList;

/**
 * @ORM\Entity
 * @ORM\Table(name="post")
 */
class Post
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     * @var \Ramsey\Uuid\UuidInterface
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    private $text;

    /**
     * @ORM\ManyToMany(targetEntity="TbBlog\Post\Tag\Tag")
     * @ORM\JoinTable(name="post_tag")
     * @ORM\OrderBy({"name" = "ASC"})
     * @var \TbBlog\Post\Tag\Tag[]|\Doctrine\Common\Collections\Collection
     */
    private $tags;

    /**
     * @ORM\Column(type="string", unique=true, length=150)
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @var \DateTimeImmutable
     */
    private $createdTime;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     * @var \DateTimeImmutable|null
     */
    private $deletedAt;

    /**
     * @ORM\OneToMany(targetEntity="TbBlog\Post\PostVisit\PostVisit", mappedBy="post")
     * @var \TbBlog\Post\PostVisit\PostVisit[]|\Doctrine\Common\Collections\Collection
     */
    private $postVisits;

    public function __construct(
        UuidInterface $id,
        string $title,
        string $text,
        TagList $tagList,
        string $slug,
        DateTimeImmutable $createdTime
    )
    {
        $this->id = $id;
        $this->title = $title;
        $this->text = $text;
        $this->tags = new ArrayCollection($tagList->getTags());
        $this->slug = $slug;
        $this->createdTime = $createdTime;
        $this->postVisits = new ArrayCollection();
    }

    public function setDeletedAt(DateTimeImmutable $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    public function unsetDeletedAt(): void
    {
        $this->deletedAt = null;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getTags(): TagList
    {
        return new TagList($this->tags->toArray());
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getCreatedTime(): DateTimeImmutable
    {
        return $this->createdTime;
    }

    public function getDeletedAt(): ?DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function isDeleted(): bool
    {
        return $this->deletedAt !== null;
    }

    public function getPostsVisits(): int
    {
        return count($this->postVisits->toArray());
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }

    public function setTags(TagList $tagList): void
    {
        $this->tags = new ArrayCollection($tagList->getTags());
    }

    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    public function setCreatedTime(DateTimeImmutable $createdTime): void
    {
        $this->createdTime = $createdTime;
    }

}
