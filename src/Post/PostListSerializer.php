<?php

declare(strict_types = 1);

namespace TbBlog\Post;

class PostListSerializer
{

    /** @var \TbBlog\Post\PostSerializer */
    private $postSerializer;

    public function __construct(
        PostSerializer $postSerializer
    )
    {
        $this->postSerializer = $postSerializer;
    }

    /**
     * @param \TbBlog\Post\PostList $postList
     * @return mixed[]
     */
    public function serializePostList(PostList $postList): array
    {
        $serializedPosts = [];

        foreach ($postList->getPosts() as $post) {
            $serializedPosts[] = $this->postSerializer->serializePost($post);
        }

        return $serializedPosts;
    }

}
