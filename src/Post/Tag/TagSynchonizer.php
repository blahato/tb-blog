<?php

declare(strict_types = 1);

namespace TbBlog\Post\Tag;

use Cocur\Slugify\Slugify;
use TbBlog\String\StringList;

class TagSynchonizer
{

    /** @var \TbBlog\Post\Tag\TagRepository */
    private $tagRepository;

    /** @var \Cocur\Slugify\Slugify */
    private $slugify;

    /** @var \TbBlog\Post\Tag\TagFactory */
    private $tagFactory;

    public function __construct(
        TagRepository $tagRepository,
        Slugify $slugify,
        TagFactory $tagFactory
    )
    {
        $this->tagRepository = $tagRepository;
        $this->slugify = $slugify;
        $this->tagFactory = $tagFactory;
    }

    public function synchronizeTagListWithDatabase(StringList $stringList): TagList
    {
        $tags = [];

        foreach ($stringList->getStrings() as $string) {
            $tags[] = $this->synchronizeTagWithDatabase($string);
        }

        return new TagList($tags);
    }

    public function synchronizeTagWithDatabase(string $tagName): Tag
    {
        $slug = $this->slugify->slugify($tagName);

        try {
            return $this->tagRepository->getTagBySlug($slug);
        } catch (\Doctrine\ORM\NoResultException $e) {
            $tag = $this->tagFactory->createTag(
                $tagName,
                $slug
            );

            return $this->tagRepository->saveTag($tag);
        }
    }

}
