<?php

declare(strict_types = 1);

namespace TbBlog\Post\Tag;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="tag")
 */
class Tag
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     * @var \Ramsey\Uuid\UuidInterface
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="string", unique=true)
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @var \DateTimeImmutable
     */
    private $createdAt;

    public function __construct(
        UuidInterface $id,
        string $name,
        string $slug,
        DateTimeImmutable $createdAt
    )
    {
        $this->id = $id;
        $this->name = $name;
        $this->slug = $slug;
        $this->createdAt = $createdAt;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

}
