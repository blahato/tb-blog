<?php

declare(strict_types = 1);

namespace TbBlog\Post\Tag;

use Ramsey\Uuid\Uuid;
use Tuscanicz\DateTimeBundle\DateTimeFactory;

class TagFactory
{

    /** @var \Tuscanicz\DateTimeBundle\DateTimeFactory */
    private $dateTimeFactory;

    public function __construct(
        DateTimeFactory $dateTimeFactory
    )
    {
        $this->dateTimeFactory = $dateTimeFactory;
    }

    public function createTag(
        string $name,
        string $slug
    ): Tag
    {
        return new Tag(
            Uuid::uuid4(),
            $name,
            $slug,
            $this->dateTimeFactory->now()->toDateTimeImmutable()
        );
    }

}
