<?php

declare(strict_types = 1);

namespace TbBlog\Post\Tag;

use Consistence\Type\Type;

class TagList
{

    /** @var \TbBlog\Post\Tag\Tag[] */
    private $tags;

    /**
     * @param \TbBlog\Post\Tag\Tag[] $tags
     */
    public function __construct(
        array $tags
    )
    {
        Type::checkType($tags, Tag::class . '[]');
        $this->tags = $tags;
    }

    /**
     * @return \TbBlog\Post\Tag\Tag[]
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    public function getTagsCount(): int
    {
        return count($this->tags);
    }

    public function hasTags(): bool
    {
        return $this->getTagsCount() > 0;
    }

}
