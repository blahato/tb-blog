<?php

declare(strict_types = 1);

namespace TbBlog\Post\Tag;

class TagSerializer
{

    /**
     * @param \TbBlog\Post\Tag\Tag $tag
     * @return string[]
     */
    public function serializeTag(Tag $tag): array
    {
        return [
            'slug' => $tag->getSlug(),
            'name' => $tag->getName(),
        ];
    }

}
