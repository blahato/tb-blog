<?php

declare(strict_types = 1);

namespace TbBlog\Post\Tag;

class TagListSerializer
{

    /** @var \TbBlog\Post\Tag\TagSerializer */
    private $tagSerializer;

    public function __construct(
        TagSerializer $tagSerializer
    )
    {
        $this->tagSerializer = $tagSerializer;
    }

    /**
     * @param \TbBlog\Post\Tag\TagList $tagList
     * @return mixed[]
     */
    public function serializeTagList(TagList $tagList): array
    {
        $serializedTags = [];

        foreach ($tagList->getTags() as $tag) {
            $serializedTags[] = $this->tagSerializer->serializeTag($tag);
        }

        return $serializedTags;
    }

}
