<?php

declare(strict_types = 1);

namespace TbBlog\Post;

use TbBlog\Post\Tag\TagListSerializer;

class PostSerializer
{

    /** @var \TbBlog\Post\Tag\TagListSerializer */
    private $tagListSerializer;

    public function __construct(
        TagListSerializer $tagListSerializer
    )
    {
        $this->tagListSerializer = $tagListSerializer;
    }

    /**
     * @param \TbBlog\Post\Post $post
     * @return mixed[]
     */
    public function serializePost(Post $post): array
    {
        return [
            'slug' => $post->getSlug(),
            'title' => $post->getTitle(),
            'text' => $post->getText(),
            'createdTime' => $post->getCreatedTime()->format('Y-m-d H:i'),
            'tags' => $this->tagListSerializer->serializeTagList($post->getTags()),
        ];
    }

}
