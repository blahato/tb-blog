<?php

declare(strict_types = 1);

namespace TbBlog\Post;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use TbBlog\Paginator\Paginator;

class PostRepository
{

    /** @var \Doctrine\ORM\EntityManagerInterface */
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    public function savePost(Post $post): Post
    {
        $this->entityManager->persist($post);
        $this->entityManager->flush();

        return $post;
    }

    public function getAllPosts(): PostList
    {
        $posts = $this->entityManager->createQueryBuilder()
            ->select('post')
            ->from(Post::class, 'post')
            ->orderBy('post.createdTime', Criteria::DESC)
            ->getQuery()
            ->getResult();

        return new PostList($posts);
    }

    public function getPublicPostsPaginated(Paginator $paginator): PostList
    {
        $posts = $this->entityManager->createQueryBuilder()
            ->select('post')
            ->from(Post::class, 'post')
            ->where('post.deletedAt IS NULL')
            ->orderBy('post.createdTime', Criteria::DESC)
            ->setFirstResult($paginator->getCurrentPageOffset())
            ->setMaxResults($paginator->getItemsPerPage())
            ->getQuery()
            ->getResult();

        return new PostList($posts);
    }

    public function getPublicPostsCount(): int
    {
        return (int) $this->entityManager->createQueryBuilder()
            ->select('COUNT(post.id)')
            ->from(Post::class, 'post')
            ->where('post.deletedAt IS NULL')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function checkPostBySlugExists(string $slug): bool
    {
        try {
            $this->getPostBySlug($slug);
            return true;
        } catch (\TbBlog\Post\Exception\PostNotFoundBySlugException $e) {
            return false;
        }
    }

    public function getPostBySlug(string $slug): Post
    {
        try {
            return $this->entityManager->createQueryBuilder()
                ->select('post')
                ->from(Post::class, 'post')
                ->where('post.slug = :slug')
                ->setParameter('slug', $slug)
                ->getQuery()
                ->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            throw new \TbBlog\Post\Exception\PostNotFoundBySlugException(
                sprintf(
                    'Post with slug "%s" does not exist',
                    $slug
                )
            );
        }
    }

    public function getPublicPostBySlug(string $slug): Post
    {
        try {
            return $this->entityManager->createQueryBuilder()
                ->select('post')
                ->from(Post::class, 'post')
                ->where('post.slug = :slug')
                ->setParameter('slug', $slug)
                ->andWhere('post.deletedAt IS NULL')
                ->getQuery()
                ->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            throw new \TbBlog\Post\Exception\PostNotFoundBySlugException(
                sprintf(
                    'Post with slug "%s" does not exist',
                    $slug
                )
            );
        }
    }

}
