<?php

declare(strict_types = 1);

namespace TbBlog\Post;

use DateTimeImmutable;
use Ramsey\Uuid\Uuid;
use TbBlog\Post\Tag\TagList;

class PostFactory
{

    public function createPost(
        string $title,
        string $text,
        TagList $tagList,
        string $slug,
        DateTimeImmutable $createdTime
    ): Post
    {
        return new Post(
            Uuid::uuid4(),
            $title,
            $text,
            $tagList,
            $slug,
            $createdTime
        );
    }

}
