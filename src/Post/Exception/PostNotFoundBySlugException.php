<?php

declare(strict_types = 1);

namespace TbBlog\Post\Exception;

class PostNotFoundBySlugException extends \TbBlog\Exception\PhpException
{

}
