<?php

declare(strict_types = 1);

namespace TbBlog\Post\Slug\Validation;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use TbBlog\Admin\Post\EditPostRequest;
use TbBlog\Admin\Post\NewPostRequest;
use TbBlog\Post\Post;
use TbBlog\Post\PostFacade;

class UniqueSlugValidator extends ConstraintValidator
{

    /** @var \TbBlog\Post\PostFacade */
    private $postFacade;

    public function __construct(
        PostFacade $postFacade
    )
    {
        $this->postFacade = $postFacade;
    }

    // @codingStandardsIgnoreStart no typehints, must match the interface
    public function validate($value, Constraint $constraint) // @codingStandardsIgnoreEnd
    {
        if (!$constraint instanceof UniqueSlug) {
            throw new \Symfony\Component\Validator\Exception\UnexpectedTypeException($constraint, UniqueSlug::class);
        }

        if ($value === null) {
            return;
        }

        if ($this->postFacade->checkPostBySlugExists($value) === true) {
            $postBySlug = $this->postFacade->getPostBySlug($value);

            if ($this->isCreatingNewPost() === true) {
                $this->addSlugExistsError($this->context);
            } else {
                $post = $this->getPost();

                if ($postBySlug->getId()->equals($post->getId()) === false) {
                    $this->addSlugExistsError($this->context);
                }
            }
        }
    }

    private function addSlugExistsError(ExecutionContextInterface $context): void
    {
        $violationMessage = 'This slug is already used.';
        $context->buildViolation($violationMessage)
            ->atPath('slug')
            ->addViolation();
    }

    private function isCreatingNewPost(): bool
    {
        return $this->context->getObject() instanceof NewPostRequest;
    }

    private function getPost(): Post
    {
        $object = $this->context->getObject();

        if ($object === null) {
            throw new \TbBlog\Post\Slug\Validation\Exception\InvalidConstraintClassException();
        }

        if ($object instanceof EditPostRequest) {
            return $object->post;
        }

        throw new \InvalidArgumentException(
            sprintf(
                'Context should contain object of %s but contains %s',
                EditPostRequest::class,
                get_class($object)
            )
        );
    }

}
