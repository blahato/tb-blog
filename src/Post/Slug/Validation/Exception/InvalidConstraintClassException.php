<?php

declare(strict_types = 1);

namespace TbBlog\Post\Slug\Validation\Exception;

class InvalidConstraintClassException extends \TbBlog\Exception\PhpException
{

}
