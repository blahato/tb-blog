<?php

declare(strict_types = 1);

namespace TbBlog\Post\Slug\Validation;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UniqueSlug extends Constraint
{

    public function validatedBy(): string
    {
        return UniqueSlugValidator::class;
    }

}
