<?php

declare(strict_types = 1);

namespace TbBlog\Post;

use TbBlog\Paginator\Paginator;
use Tuscanicz\DateTimeBundle\DateTimeFactory;

class PostFacade
{

    /** @var \TbBlog\Post\PostRepository */
    private $postRepository;

    /** @var \Tuscanicz\DateTimeBundle\DateTimeFactory */
    private $dateTimeFactory;

    public function __construct(
        PostRepository $postRepository,
        DateTimeFactory $dateTimeFactory
    )
    {
        $this->postRepository = $postRepository;
        $this->dateTimeFactory = $dateTimeFactory;
    }

    public function getAllPosts(): PostList
    {
        return $this->postRepository->getAllPosts();
    }

    public function getPublicPostsPaginated(Paginator $paginator): PostList
    {
        return $this->postRepository->getPublicPostsPaginated($paginator);
    }

    public function getPublicPostsCount(): int
    {
        return $this->postRepository->getPublicPostsCount();
    }

    public function deletePost(Post $post): Post
    {
        $post->setDeletedAt(
            $this->dateTimeFactory->now()->toDateTimeImmutable()
        );

        return $this->postRepository->savePost($post);
    }

    public function publishPost(Post $post): Post
    {
        $post->unsetDeletedAt();
        return $this->postRepository->savePost($post);
    }

    public function getPostBySlug(string $slug): Post
    {
        return $this->postRepository->getPostBySlug($slug);
    }

    public function getPublicPostBySlug(string $slug): Post
    {
        return $this->postRepository->getPublicPostBySlug($slug);
    }

    public function checkPostBySlugExists(string $slug): bool
    {
        return $this->postRepository->checkPostBySlugExists($slug);
    }

    public function savePost(Post $post): Post
    {
        return $this->postRepository->savePost($post);
    }

}
