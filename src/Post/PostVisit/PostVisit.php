<?php

declare(strict_types = 1);

namespace TbBlog\Post\PostVisit;

use Consistence\Doctrine\Enum\EnumAnnotation as Enum;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use TbBlog\Post\Post;

/**
 * @ORM\Entity
 * @ORM\Table(name="post_visit")
 */
class PostVisit
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     * @var \Ramsey\Uuid\UuidInterface
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TbBlog\Post\Post", inversedBy="postVisits")
     * @var \TbBlog\Post\Post
     */
    private $post;

    /**
     * @Enum(class=TbBlog\Post\PostVisit\PostVisitSourceEnum::class)
     * @ORM\Column(type="string_enum")
     * @var \TbBlog\Post\PostVisit\PostVisitSourceEnum
     */
    private $source;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @var \DateTimeImmutable
     */
    private $createdAt;

    public function __construct(
        UuidInterface $id,
        Post $post,
        PostVisitSourceEnum $postVisitSource,
        DateTimeImmutable $createdAt
    )
    {
        $this->id = $id;
        $this->post = $post;
        $this->source = $postVisitSource;
        $this->createdAt = $createdAt;
    }

    public function getPost(): Post
    {
        return $this->post;
    }

}
