<?php

declare(strict_types = 1);

namespace TbBlog\Post\PostVisit;

use Consistence\Enum\Enum;

class PostVisitSourceEnum extends Enum
{

    public const WEB = 'web';
    public const API = 'api';

}
