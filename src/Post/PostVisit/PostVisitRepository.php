<?php

declare(strict_types = 1);

namespace TbBlog\Post\PostVisit;

use Doctrine\ORM\EntityManagerInterface;

class PostVisitRepository
{

    /** @var \Doctrine\ORM\EntityManagerInterface */
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    public function savePostVisit(PostVisit $postVisit): PostVisit
    {
        $this->entityManager->persist($postVisit);
        $this->entityManager->flush();

        return $postVisit;
    }

}
