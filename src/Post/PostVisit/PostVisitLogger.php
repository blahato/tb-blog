<?php

declare(strict_types = 1);

namespace TbBlog\Post\PostVisit;

use TbBlog\Post\Post;

class PostVisitLogger
{

    /** @var \TbBlog\Post\PostVisit\PostVisitFacade */
    private $postVisitFacade;

    /** @var \TbBlog\Post\PostVisit\PostVisitFactory */
    private $postVisitFactory;

    public function __construct(
        PostVisitFacade $postVisitFacade,
        PostVisitFactory $postVisitFactory
    )
    {
        $this->postVisitFacade = $postVisitFacade;
        $this->postVisitFactory = $postVisitFactory;
    }

    public function logWebVisit(Post $post): PostVisit
    {
        $postVisit = $this->postVisitFactory->createPostVisit(
            $post,
            PostVisitSourceEnum::get(PostVisitSourceEnum::WEB)
        );

        return $this->postVisitFacade->savePostVisit($postVisit);
    }

    public function logApiVisit(Post $post): PostVisit
    {
        $postVisit = $this->postVisitFactory->createPostVisit(
            $post,
            PostVisitSourceEnum::get(PostVisitSourceEnum::API)
        );

        return $this->postVisitFacade->savePostVisit($postVisit);
    }

}
