<?php

declare(strict_types = 1);

namespace TbBlog\Post\PostVisit;

use Ramsey\Uuid\Uuid;
use TbBlog\Post\Post;
use Tuscanicz\DateTimeBundle\DateTimeFactory;

class PostVisitFactory
{

    /** @var \Tuscanicz\DateTimeBundle\DateTimeFactory */
    private $dateTimeFactory;

    public function __construct(
        DateTimeFactory $dateTimeFactory
    )
    {
        $this->dateTimeFactory = $dateTimeFactory;
    }

    public function createPostVisit(
        Post $post,
        PostVisitSourceEnum $postVisitSource
    ): PostVisit
    {
        return new PostVisit(
            Uuid::uuid4(),
            $post,
            $postVisitSource,
            $this->dateTimeFactory->now()->toDateTimeImmutable()
        );
    }

}
