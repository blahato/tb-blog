<?php

declare(strict_types = 1);

namespace TbBlog\Post\PostVisit;

class PostVisitFacade
{

    /** @var \TbBlog\Post\PostVisit\PostVisitRepository */
    private $postVisitRepository;

    public function __construct(
        PostVisitRepository $postVisitRepository
    )
    {
        $this->postVisitRepository = $postVisitRepository;
    }

    public function savePostVisit(PostVisit $postVisit): PostVisit
    {
        return $this->postVisitRepository->savePostVisit($postVisit);
    }

}
