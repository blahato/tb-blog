<?php

declare(strict_types = 1);

namespace TbBlog\Post;

class PostList
{

    /** @var \TbBlog\Post\Post[] */
    private $posts;

    /**
     * @param \TbBlog\Post\Post[] $posts
     */
    public function __construct(
        array $posts
    )
    {
        $this->posts = $posts;
    }

    public function getPostsCount(): int
    {
        return count($this->posts);
    }

    public function hasPosts(): bool
    {
        return $this->getPostsCount() > 0;
    }

    /**
     * @return \TbBlog\Post\Post[]
     */
    public function getPosts(): array
    {
        return $this->posts;
    }

}
