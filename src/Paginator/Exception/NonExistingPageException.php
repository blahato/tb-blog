<?php

declare(strict_types = 1);

namespace TbBlog\Paginator\Exception;

class NonExistingPageException extends \TbBlog\Exception\PhpException
{

}
