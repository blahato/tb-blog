<?php

declare(strict_types = 1);

namespace TbBlog\Paginator\Exception;

class ZeroItemsPerPageException extends \TbBlog\Exception\PhpException
{

}
