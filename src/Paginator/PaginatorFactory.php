<?php

declare(strict_types = 1);

namespace TbBlog\Paginator;

class PaginatorFactory
{

    public function createPaginator(
        int $itemsCount,
        int $itemsPerPage,
        int $currentPage
    ): Paginator
    {
        return new Paginator(
            $itemsCount,
            $itemsPerPage,
            $currentPage
        );
    }

}
