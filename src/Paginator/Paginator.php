<?php

declare(strict_types = 1);

namespace TbBlog\Paginator;

class Paginator
{

    /** @var int */
    private $itemsCount;

    /** @var int */
    private $itemsPerPage;

    /** @var int */
    private $currentPage;

    public function __construct(
        int $itemsCount,
        int $itemsPerPage,
        int $currentPage
    )
    {
        $this->itemsCount = $itemsCount;
        $this->itemsPerPage = $itemsPerPage;
        $this->currentPage = $currentPage;

        if ($itemsPerPage === 0) {
            throw new \TbBlog\Paginator\Exception\ZeroItemsPerPageException('Cannot create paginator with 0 items per page.');
        }

        if ($currentPage === 0) {
            throw new \TbBlog\Paginator\Exception\NonExistingPageException(
                'Page number 0 cannot be accessed.'
            );
        }

        if ($this->currentPage > $this->getNumberOfPages()) {
            throw new \TbBlog\Paginator\Exception\NonExistingPageException(
                sprintf(
                    'Page number %d does not exists. Number of pages is %d.',
                    $currentPage,
                    $this->getNumberOfPages()
                )
            );
        }
    }

    public function getItemsPerPage(): int
    {
        return $this->itemsPerPage;
    }

    public function getCurrentPageOffset(): int
    {
        return $this->itemsPerPage * ($this->currentPage - 1);
    }

    public function getNumberOfPages(): int
    {
        return $this->itemsCount === 0 ? 1 : (int) ceil($this->itemsCount / $this->itemsPerPage);
    }

}
